﻿using System;
using Logging_Task;

namespace AdditionAlgorithm
{
    public class AddAlgorithm : IAlgorithm
    {
        /// <summary>
        /// calculate summ.
        /// </summary>
        /// <param name="first">first int argument.</param>
        /// <param name="second">second int argument.</param>
        /// <returns>Summ.</returns>
        public int Calculate(int first, int second)
        {
            return first + second;
        }    
    }
}
