﻿using Microsoft.Extensions.Logging;
using NUnit.Framework;
using NLog.Extensions.Logging;
using Moq;
using Logging_Task;
using AdditionAlgorithm;

namespace Algorithm.Tests
{
    public class AlgorithmTests
    {
        ILoggerFactory loggerFactory;

        [SetUp]
        public void Setup()
        {
            loggerFactory = LoggerFactory.Create(builder => { builder.AddNLog().SetMinimumLevel(LogLevel.Trace); });
        }

        [TestCase(3, 3, ExpectedResult = 6)]
        [TestCase(200, 134, ExpectedResult = 334)]
        [TestCase(423, 423, ExpectedResult = 846)]
        public int GetSumm(int a, int b)
        {
            LoggerExtendedAlgorithm algorithm = new LoggerExtendedAlgorithm(new AddAlgorithm(), loggerFactory.CreateLogger<IAlgorithm>());
            return algorithm.Calculate(a, b);
        }

        [Test]
        public void AlgorithmMoqTest()
        {
            var moq = new Mock<IAlgorithm>();
            moq.Setup<int>(ld => ld.Calculate(0, 0));
            IAlgorithm algorithm = moq.Object;
            for (int i = 0; i < 100; i++)
            {
                algorithm.Calculate(0, 0);
            }

            moq.Verify(ld => ld.Calculate(0, 0), Times.Exactly(100));
        }
    }
}
