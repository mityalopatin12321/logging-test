﻿using System;

namespace Logging_Task
{
    /// <summary>      
    /// Provides templates for some algorithm calculating.      
    /// </summary>      
    public interface IAlgorithm
    {
        /// <summary>
        /// Do some calculations.
        /// </summary>
        /// <param name="first">first int argument.</param>
        /// <param name="second">second int argument.</param>
        /// <returns>Result of calculations.</returns>
        public int Calculate(int first, int second);
    }
}
