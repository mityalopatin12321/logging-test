﻿using System;
using Microsoft.Extensions.Logging;

namespace Logging_Task
{
    public class LoggerExtendedAlgorithm : IAlgorithm
    {
        private readonly IAlgorithm algorithm;

        private readonly ILogger<IAlgorithm> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimerExtendedAlgorithm"/> class.
        /// </summary>
        /// <param name="algorithm">Algorithm.</param>
        /// <param name="logger">Algorithm.</param>
        /// <exception cref="ArgumentNullException">Throw when algorithm or logger is null.</exception>
        public LoggerExtendedAlgorithm(IAlgorithm algorithm, ILogger<IAlgorithm> logger)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            if(algorithm is null)
            {
                logger.LogError("Fail to initialize TimerExtendedAlgorithm.");
                throw new ArgumentNullException(nameof(algorithm));
            }
            this.algorithm = algorithm;
        }

        /// <summary>
        /// Do some calculations.
        /// </summary>
        /// <param name="first">first int argument.</param>
        /// <param name="second">second int argument.</param>
        /// <returns>Result of calculations.</returns>
        public int Calculate(int first, int second)
        {
            if(first<0||second<0)
            {
                logger.LogWarning("One or both of arguments are negative");
            }
            int answer = algorithm.Calculate(first, second);
            logger.LogTrace($"Perform Calculate method with {answer} result.");
            return answer;
        }
    }
}
