﻿using System;
using System.Diagnostics;

namespace Logging_Task
{
    public class TimerExtendedAlgorithm : IAlgorithm
    {
        private readonly IAlgorithm algorithm;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggerExtendedAlgorithm"/> class.
        /// </summary>
        /// <param name="algorithm">Algorithm.</param>
        /// <exception cref="ArgumentNullException">Throw when algorithm is null.</exception>
        public TimerExtendedAlgorithm(IAlgorithm algorithm) => this.algorithm = algorithm ?? throw new ArgumentNullException(nameof(algorithm));

        /// <summary>
        /// Gets the running time of the algotithm.
        /// </summary>
        public long Milliseconds { get; private set; }

        /// <summary>
        /// Do some calculations.
        /// </summary>
        /// <param name="first">first int argument.</param>
        /// <param name="second">second int argument.</param>
        /// <returns>Result of calculations.</returns>
        public int Calculate(int first, int second)
        {
            Stopwatch timePerParse = new Stopwatch();
            timePerParse.Start();
            int result = this.algorithm.Calculate(first, second);
            timePerParse.Stop();
            Milliseconds = timePerParse.ElapsedMilliseconds;
            return result;
        }
    }
}
